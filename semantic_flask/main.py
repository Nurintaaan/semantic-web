from flask import Flask, render_template
from semantic_flask.controllers import get_planets

app = Flask(__name__)


@app.route("/")
def home_page():
    planet = get_planets()
    return render_template("index.html", planets=planet)


@app.errorhandler(404)
def not_found(error):
    return render_template('404.html'), 404
