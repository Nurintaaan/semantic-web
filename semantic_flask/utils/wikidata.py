import requests

QUERY_MAP = {
    "constellation_name": " FILTER(CONTAINS(LCASE(?consLabel), '{}')).",
    # "star": " FILTER(CONTAINS(LCASE(?starLabel), '{}')).",
    # "discovery method": " FILTER(CONTAINS(LCASE(?discoveryLabel), '{}')).",
    "discovery_year_bigger": " FILTER (?date >= '{}-01-01T00:00:00Z'^^xsd:dateTime).",
    "discovery_year_less": " FILTER (?date <= '{}-01-01T00:00:00Z'^^xsd:dateTime)."
}


def query_exoplanet(query_property={}):
    url = "https://query.wikidata.org/sparql"
    additional_query = ""
    for key in query_property:
        additional_query += QUERY_MAP[key].format(query_property[key].lower())
    query = """
SELECT ?planet ?planetLabel ?cons ?consLabel ?star ?starLabel ?date ?discovery ?discoveryLabel
WHERE
{
    ?planet wdt:P31 wd:Q44559.
    ?planet rdfs:label ?planetLabel.
    FILTER(LANG(?planetLabel) = "en").
    %s
    OPTIONAL {
        ?planet wdt:P575 ?date.
    }
    OPTIONAL {
        ?planet wdt:P59 ?cons.
        ?cons rdfs:label ?consLabel.
        FILTER(LANG(?consLabel) = "en").
    }
    OPTIONAL {
        ?planet wdt:P397 ?star.
        ?star rdfs:label ?starLabel.
        FILTER(LANG(?starLabel) = "en").
    }
    OPTIONAL {
        ?planet wdt:P1046 ?discovery.
        ?discovery rdfs:label ?discoveryLabel.
        FILTER(LANG(?discoveryLabel) = "en").
    }
}
    """ % additional_query
    r = requests.get(url, params={"format": "json", "query": query})
    result = {}
    try:
        data = r.json()
        for i in data['results']['bindings']:
            result[i['planetLabel']['value']] = {
                'planet_url': i['planet']['value'],
                'constellation_name': i.get("consLabel", {'value': ''})['value'],
                'constellation_url': i.get("cons", {'value': ''})['value'],
                # 'star': i.get("consLabel", {'value': ''})['value'],
                'star_url': i.get("cons", {'value': ''})['value'],
                # 'discovery_method': i.get("discoveryLabel", {'value': ''})['value'],
                'discovery_method_url':  i.get("discovery", {'value': ''})['value'],
                'discovery_date': i.get("date", {'value': ''})['value'][0:10],
            }
    except ValueError:
        print("not found")
    finally:
        return result


def planet_only():
    query = """
SELECT ?planet ?planetLabel
WHERE
{
    ?planet wdt:P31 wd:Q44559.
    SERVICE wikibase:label { bd:serviceParam wikibase:language "en". }

}
"""
    r = requests.get(url, params={"format": "json", "query": query})
    try:
        data = r.json()
        result = {}
        for i in data['results']['bindings']:
            result[i['planetLabel']['value']] = i['planet']['value']
    except ValueError:
        print("not found")
    finally:
        return result
