import rdflib
from .utils.wikidata import query_exoplanet, QUERY_MAP


def query_planets(where_query={}, page=None, num_planet=10):
    page_query = ""

    # pagination
    if page is not None:
        page_query = "OFFSET {}".format(num_planet * (page - 1))

    columns = [
        "planet",
        "pl_hostname",
        "pl_name",
        "pl_discmethod",
        "pl_pnum",
        "pl_orbper",
        "pl_orbeccen",
        "pl_bmassj",
        "pl_radj",
        "pl_dens",
        "pl_nnotes",
        "ra",
        "dec",
        "st_dist",
        "gaia_gmag",
        "st_teff",
        "pl_facility"
    ]

    strSelect = ""
    strWhere = ""
    for column in columns:
        strSelect += "?{}\n".format(column)
        if(column in where_query):
            strWhere += " FILTER(CONTAINS(LCASE(?{}), '{}')).\n".\
                format(column, where_query[column].lower())
    g = rdflib.Graph()
    wikidata_columns = [
        "constellation_name",
        "discovery_date",
        "planet_url",
        "constellation_url",
        "star_url",
        "discovery_method_url"
    ]
    wikidata_query = {}
    for column in QUERY_MAP:
        if(column in where_query):
            wikidata_query[column] = where_query[column]
    wikidata = query_exoplanet(wikidata_query)
    if(len(wikidata_query) > 0):
        strWhere += " FILTER(?pl_name in ({}))".\
            format(", ".join(['"' + i + '"' for i in wikidata]))
    g.parse("CSVtoRDF/planet.ttl", format='turtle')

    query = """BASE   <http://example.org/>
             PREFIX wd: <http://www.wikidata.org/entity/>
             PREFIX p: <http://www.examplepath.org/>
             SELECT
                %s
             WHERE {
               ?planet p:pl_hostname ?pl_hostname;
                         p:pl_name ?pl_name;
                         p:pl_discmethod ?pl_discmethod;
                         p:pl_pnum ?pl_pnum;
                         p:pl_orbper ?pl_orbper;
                         p:pl_orbeccen ?pl_orbeccen;
                         p:pl_bmassj ?pl_bmassj;
                         p:pl_radj ?pl_radj;
                         p:pl_dens ?pl_dens;
                         p:pl_nnotes ?pl_nnotes;
                         p:ra ?ra;
                         p:dec ?dec;
                         p:st_dist ?st_dist;
                         p:gaia_gmag ?gaia_gmag;
                         p:st_teff ?st_teff;
                         p:pl_facility ?pl_facility.
                %s
             }
             """ % (strSelect, strWhere)
    result = g.query(query)
    planet_data = {}
    if(len(result) <= 0):
        for i in columns:
            planet_data[i] = []
        for i in wikidata_columns:
            planet_data[i] = []
        return planet_data
    for column in columns:
        for row in result:
            if column not in planet_data.keys():
                planet_data[column] = []
            planet_data[column].append(row[column])
    for key in planet_data.get("pl_name", []):
        for column in wikidata_columns:
            if column not in planet_data.keys():
                planet_data[column] = []
            planet_data[column].append(wikidata.get(str(key), {column: ""})[column])

    return planet_data

def get_planets_from_query(planet_data):
    planets = []
    for idx, planet in enumerate(planet_data['planet']):
        planet = {}
        for key in planet_data.keys():
            planet[key] = planet_data[key][idx]
        planets.append(planet)
    return planets

def get_planets():
    planet_data = query_planets()
    return get_planets_from_query(planet_data)
