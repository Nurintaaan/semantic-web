# Semantic Web


Cara pake utils.wikidata.query_exoplanet:

buat dict yang isinya query
key2nya:
"planet"
"constellation"
"star"
"discovery method"
"discovery year bigger"
"discovery year less"
Kalau gamau query samsek tinggal masukin query_exoplanet({})
Kalau mau wajib ada starnya (gasemua planet ada nama starnya) tapi gamau query star. tinggal masukin ({'star': ''})
Untuk date itu year bigger >= dan year less <=
ini searchnya pake contains
return value:
dict yang keynya nama planet dan valuenya dictionary, isi2 dictionary valuenya:
'url'
'constellation'
'constellation url'
'star'
'star url'
'discovery method'
'discovery method url'
'discovery date'

note: kalau ga ada valuenya keynya tetep ada tapi return valuenya bakal string kosong
ini berlaku juga buat yang discovery date

http://www.paulbrownmagic.com/blog/flask_foaf

QUERY BY NAME

SELECT ?planetLabel ?consLabel ?starLabel ?date ?discovery ?discoveryLabel
WHERE
{
    ?planet wdt:P31 wd:Q44559.
    ?planet rdfs:label ?planetLabel.
    FILTER(LANG(?planetLabel) = "en").
    OPTIONAL {
        ?planet wdt:P575 ?date.
    }
    OPTIONAL {
        ?planet wdt:P59 ?cons.
        ?cons rdfs:label ?consLabel.
        FILTER(LANG(?consLabel) = "en").
    }
    OPTIONAL {
        ?planet wdt:P397 ?star.
        ?star rdfs:label ?starLabel.
        FILTER(LANG(?starLabel) = "en").
    }
    OPTIONAL {
        ?planet wdt:P1046 ?discovery.
        ?discovery rdfs:label ?discoveryLabel.
        FILTER(LANG(?discoveryLabel) = "en").
    }
}


QUERY BY CONS // OR star, OR discovery

SELECT ?planet ?planetLabel ?cons ?consLabel ?star ?starLabel ?date ?discovery ?discoveryLabel
WHERE 
{
	?planet wdt:P31 wd:Q44559.
	?planet wdt:P59 ?cons.
	?planet rdfs:label ?planetLabel.
	?cons rdfs:label ?consLabel.
	FILTER(LANG(?planetLabel) = "en").
	FILTER(LANG(?consLabel) = "en").
	FILTER(CONTAINS(LCASE(?consLabel), "{}")).
    OPTIONAL {
        ?planet wdt:P575 ?date.
    }
    OPTIONAL {
        ?planet wdt:P397 ?star.
        ?star rdfs:label ?starLabel.
        FILTER(LANG(?starLabel) = "en").
    }
    OPTIONAL {
        ?planet wdt:P1046 ?discovery.
        ?discovery rdfs:label ?discoveryLabel.
        FILTER(LANG(?discoveryLabel) = "en").
    }
}

QUERY BY DATE

SELECT ?planet ?planetLabel ?cons ?consLabel ?star ?starLabel ?date ?discovery ?discoveryLabel
WHERE 
{
	?planet wdt:P31 wd:Q44559.
	?planet wdt:P575 ?date.
	?planet rdfs:label ?planetLabel.
	FILTER(LANG(?planetLabel) = "en").
	FILTER (?date > "1917-01-01T00:00:00Z"^^xsd:dateTime).
	    OPTIONAL {
        ?planet wdt:P59 ?cons.
        ?cons rdfs:label ?consLabel.
        FILTER(LANG(?consLabel) = "en").
    }
    OPTIONAL {
        ?planet wdt:P397 ?star.
        ?star rdfs:label ?starLabel.
        FILTER(LANG(?starLabel) = "en").
    }
    OPTIONAL {
        ?planet wdt:P1046 ?discovery.
        ?discovery rdfs:label ?discoveryLabel.
        FILTER(LANG(?discoveryLabel) = "en").
    }
}